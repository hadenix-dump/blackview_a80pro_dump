#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/recovery:15401888:984806c25bad3f082684f7cc4b7bf04b5c664896; then
  applypatch  EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/boot:9153440:a9a797f73f3d3da6f7bedd35385e0994d91ee8d0 EMMC:/dev/block/platform/mtk-msdc.0/11230000.msdc0/by-name/recovery 984806c25bad3f082684f7cc4b7bf04b5c664896 15401888 a9a797f73f3d3da6f7bedd35385e0994d91ee8d0:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
